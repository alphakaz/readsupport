class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.string :category
      t.string :label
      t.integer :year
      t.text :outline
      t.text :memo

      t.timestamps
    end
  end
end

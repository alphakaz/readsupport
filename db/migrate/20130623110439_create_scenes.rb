class CreateScenes < ActiveRecord::Migration
  def change
    create_table :scenes do |t|
      t.integer :book_id
      t.string :title
      t.text :outline

      t.timestamps
    end
  end
end

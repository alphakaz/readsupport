json.array!(@books) do |book|
  json.extract! book, :title, :author, :category, :label, :year, :outline, :memo
  json.url book_url(book, format: :json)
end

json.array!(@scenes) do |scene|
  json.extract! scene, :book_id, :title, :outline
  json.url scene_url(scene, format: :json)
end
